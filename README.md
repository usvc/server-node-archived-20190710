# Server (Node)
A cloud-native server boilerplate that includes cross-origin request sharing support, content security policy support, metrics, and tracing.

- [Configuration](./docs/config.md)
- [Contributing](./docs/contributing.md)

# Usage

## Example
An example can be found by running:

```sh
npm run eg
```

The example can be found [in the `./example` directory](./example).

## Pre-Provisioned Endpoints
The following endpoints will be automatically provisioned:

| Path | Description |
| --- | --- |
| `/_/report/:type` | Reporting URI endpoint for front-end browser violations (eg. CSP, Feature Policies, XSS) |
| `/_/metrics/` | Prometheus metrics reporting endpoint |
| `/_/healthz/` | Liveness healthcheck endpoint. Responds with a 200 status code on ok, 500 or no response otherwise. |
| `/_/readyz/` | Readiness healthcheck endpoint. Responds with a 200 status code on ok, 500 with errors or no response otherwise. |

# License
This project is licensed under the MIT license. See the [LICENSE file](./LICENSE) for the full text.
