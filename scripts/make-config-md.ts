import * as fs from 'fs';
import * as path from 'path';

import {configDefinition as loggerConfig} from '../src/logger-component/config';
import {configDefinition as serverConfig} from '../src/server-component/config';

const pathToConfigMd = path.join(__dirname, '../docs/config.md');
const processedVariables = [];
let output = '# Configuration Variables\n\n'
  + '| Environment Variable | Defaults To | Description |\n'
  + '| --- | --- | --- |\n';

output += Object.keys(loggerConfig)
  .map((loggerVar) => {
    const environmentVariable = loggerConfig[loggerVar].env;
    const description = loggerConfig[loggerVar].description;
    let defaultValue = loggerConfig[loggerVar].default;
    if (typeof defaultValue === 'string') {
      defaultValue = `"${defaultValue}"`;
    }
    processedVariables.push(environmentVariable);
    return `| \`${environmentVariable}\` `
      + `| \`${defaultValue}\` `
      + `| ${description} |`;
  }).join('\n');
output += Object.keys(serverConfig)
  .map((serverVar) => {
    const environmentVariable = serverConfig[serverVar].env;
    const description = serverConfig[serverVar].description;
    let defaultValue = serverConfig[serverVar].default;
    if (typeof defaultValue === 'string') {
      defaultValue = `"${defaultValue}"`;
    }
    processedVariables.push(environmentVariable);
    return `| \`${environmentVariable}\` `
      + `| \`${defaultValue}\` `
      + `| ${description} |`;
  }).join('\n');


fs.writeFileSync(pathToConfigMd, output + '\n');
process.stdout.write('processed the following variables:\n');
processedVariables.forEach((processedVariable) => {
  process.stdout.write(`- ${processedVariable}\n`);
});
process.stdout.write(`\nwritten to file at ${pathToConfigMd}\n`);
process.stdout.write('exiting now...\n');
