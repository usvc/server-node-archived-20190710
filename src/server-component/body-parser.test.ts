import {expect} from 'chai';
import * as expressPromBundle from 'express-prom-bundle';
import * as supertest from 'supertest';

import {Server} from '~';

describe('body parsing', () => {
  let server: Server;
  let testServer: supertest.SuperTest<supertest.Test>;
  const testBodyData = {
    number: 1,
    float: 1.23,
    string: '1.23',
    boolean: true,
    array: ['1', '2', '3'],
    object: {
      number: 2,
      float: 2.34,
      string: '2.34',
      boolean: false,
      array: [2, 3, 4],
    },
  };

  before(() => {
    expressPromBundle.promClient.register.clear();
    server = new Server({
      serverLogLevel: 'error',
    });
    testServer = supertest(server.ejectServer());
    server.post('/__post', (req, res) => res.json(req.body));
  });

  it('works for */json', () =>
    testServer
      .post('/__post')
      .set('Content-Type', 'application/json')
      .send(testBodyData)
      .then((res) => {
        expect(res.body).to.deep.equal(testBodyData);
      })
  );

  it('works for */x-www-form-urlencoded', () => {
    const urlEncodedData = {
      number: testBodyData.number.toString(),
      float: testBodyData.float.toString(),
      string: testBodyData.string,
      boolean: testBodyData.boolean.toString(),
      array: testBodyData.array.map((i) => i.toString()),
      object: {
        number: testBodyData.object.number.toString(),
        float: testBodyData.object.float.toString(),
        string: testBodyData.object.string,
        array: testBodyData.object.array.map((i) => i.toString()),
        boolean: testBodyData.object.boolean.toString(),
      },
    };

    return testServer
      .post('/__post')
      .set('Content-Type', 'application/x-www-form-urlencoded')
      .send(testBodyData)
      .then((res) => {
        expect(res.body).to.deep.equal(urlEncodedData);
      });
  });
});
