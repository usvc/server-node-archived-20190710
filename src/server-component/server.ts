import {Console} from 'console';
import * as http from 'http';
import * as os from 'os';
import * as path from 'path';
import * as stream from 'stream';

import * as express from 'express';
import * as morgan from 'morgan';
import * as basicAuth from 'basic-auth';
import * as cors from 'cors';
import * as helmet from 'helmet';
import * as xssFilter from 'x-xss-protection';
import * as featurePolicy from 'feature-policy';
import * as expressPromBundle from 'express-prom-bundle';
import * as uuid from 'uuid/v4';
import {Config} from 'convict';
import {Logger} from 'winston';
import * as cookieParser from 'cookie-parser';
import * as zipkin from 'zipkin';
import * as clsHooked from 'cls-hooked';
import zipkinContextCls = require('zipkin-context-cls');
import {HttpLogger} from 'zipkin-transport-http';
import {expressMiddleware as zipkinExpressMiddleware} from 'zipkin-instrumentation-express';

import {config} from './config';
import {BodyParser} from './body-parser';
import {Csp} from './csp';
import {logger} from '~/logger-component';
import * as common from '../common';
import { PathParams } from 'express-serve-static-core';

const serverSplash = '\n' +
  '                        __                                                 _       \n' +
  ' _   _ _____   _____   / /__  ___ _ ____   _____ _ __      _ __   ___   __| | ___  \n' +
  '| | | / __\\ \\ / / __| / / __|/ _ \\ \'__\\ \\ / / _ \\ \'__|____| \'_ \\ / _ \\ / _` |/ _ \\ \n' +
  '| |_| \\__ \\\\ V / (__ / /\\__ \\  __/ |   \\ V /  __/ | |_____| | | | (_) | (_| |  __/ \n' +
  ' \\__,_|___/ \\_/ \\___/_/ |___/\\___|_|    \\_/ \\___|_|       |_| |_|\\___/ \\__,_|\\___| \n' +
  '                                                                                   \n';

export type ExpressHandlerParameters = PathParams | express.RequestHandler | express.Application;

export interface ServerConfig {
  serverBodyParserEnabled?: boolean;
  serverBodyParserSizeLimit?: string;
  serverBodyParserType?: string;
  serverCookiesEnabled?: boolean;
  serverCorsEnabled?: boolean;
  serverCorsHeaderWhitelist?: string;
  serverCorsUrlWhitelist?: string;
  serverCorsHttpVerbWhitelist?: string;
  serverCspBlockMixedContent?: boolean;
  serverCspEnabled?: boolean;
  serverCspChild?: string;
  serverCspConnect?: string;
  serverCspDefault?: string;
  serverCspFont?: string;
  serverCspFrame?: string;
  serverCspImg?: string;
  serverCspManifest?: string;
  serverCspMedia?: string;
  serverCspObject?: string;
  serverCspPrefetch?: string;
  serverCspScript?: string;
  serverCspStyle?: string;
  serverCspUpgradeInsecure?: boolean;
  serverCspWorker?: string;
  serverInterface?: string;
  serverLogLevel?: string;
  serverMetricsEnabled?: boolean;
  serverMetricsUri?: string;
  serverMetricsBasicAuthEnabled?: boolean;
  serverMetricsBasicAuthCredentials?: string;
  serverPort?: number;
  serverPublicUrl?: string;
  serverTracingEnabled?: boolean;
  serverReportUri?: string;
  serverSecureHttpHeadersEnabled?: boolean;
  serverServiceId?: string;
  serverShutdownHooks?: ServerShutdownHook[];
}

export class Server {
  id: string;
  config: Config<{}>;
  instance: http.Server;
  logger: Logger;
  app: express.Application;
  onShutdown: ServerShutdownHook[];
  corsUrlWhitelist: string[];
  corsHttpVerbWhitelist: string[];
  context: clsHooked.Namespace;
  tracer: zipkin.Tracer;
  startTime: Date;

  constructor({
    serverBodyParserEnabled = config.get('serverBodyParserEnabled'),
    serverBodyParserSizeLimit = config.get('serverBodyParserSizeLimit'),
    serverBodyParserType = config.get('serverBodyParserType'),
    serverCookiesEnabled = config.get('serverCookiesEnabled'),
    serverCorsEnabled = config.get('serverCorsEnabled'),
    serverCorsHeaderWhitelist = config.get('serverCorsHeaderWhitelist'),
    serverCorsUrlWhitelist = config.get('serverCorsUrlWhitelist'),
    serverCorsHttpVerbWhitelist = config.get('serverCorsHttpVerbWhitelist'),
    serverCspEnabled = config.get('serverCspEnabled'),
    serverCspBlockMixedContent = config.get('serverCspBlockMixedContent'),
    serverCspChild = config.get('serverCspChild'),
    serverCspConnect = config.get('serverCspConnect'),
    serverCspDefault = config.get('serverCspDefault'),
    serverCspFont = config.get('serverCspFont'),
    serverCspFrame = config.get('serverCspFrame'),
    serverCspImg = config.get('serverCspImg'),
    serverCspManifest = config.get('serverCspManifest'),
    serverCspMedia = config.get('serverCspMedia'),
    serverCspObject = config.get('serverCspObject'),
    serverCspPrefetch = config.get('serverCspPrefetch'),
    serverCspScript = config.get('serverCspScript'),
    serverCspStyle = config.get('serverCspStyle'),
    serverCspUpgradeInsecure = config.get('serverCspUpgradeInsecure'),
    serverCspWorker = config.get('serverCspWorker'),
    serverInterface = config.get('serverInterface'),
    serverLogLevel = config.get('serverLogLevel'),
    serverMetricsEnabled = config.get('serverMetricsEnabled'),
    serverMetricsUri = config.get('serverMetricsUri'),
    serverMetricsBasicAuthEnabled = config.get('serverMetricsBasicAuthEnabled'),
    serverMetricsBasicAuthCredentials = config.get('serverMetricsBasicAuthCredentials'),
    serverPort = config.get('serverPort'),
    serverPublicUrl = config.get('serverPublicUrl'),
    serverTracingEnabled = config.get('serverTracingEnabled'),
    serverSecureHttpHeadersEnabled = config.get('serverSecureHttpHeadersEnabled'),
    serverServiceId = config.get('serverServiceId'),
    serverReportUri = config.get('serverReportUri'),
    serverShutdownHooks = [],
  }: ServerConfig = {}) {
    this.id = `${os.hostname()}-${uuid()}`;
    this.startTime = new Date();
    this.context = clsHooked.createNamespace(serverServiceId);

    this.logger = logger.init('server', {
      additionalFormatters: [
        logger.zipkinContextFormatter(serverServiceId),
      ],
      level: serverLogLevel,
    });
    this.logger.info(serverSplash);
    this.logger.debug('initialising server component...');
    
    // configuration management
    this.config = config;
    Object.keys(config.getProperties()).forEach((configProperty) => {
      let propertyIsDefined = false;
      try {
        eval(configProperty);
        propertyIsDefined = true;
      } catch (ex) {}
      if (propertyIsDefined) {
        this.config.set(configProperty, eval(configProperty));
        this.logger.debug(`${configProperty} has been set to "${this.config.get(configProperty)}"`);
      } else {
        this.logger.warn(`${configProperty} was not found. skipping...`);
      }
    });
    this.corsHttpVerbWhitelist =
      this.config.get('serverCorsHttpVerbWhitelist')
        .split(',').map((verb) => verb.toLowerCase());

    // application creation
    this.app = express();

    // body handling
    if (serverBodyParserEnabled) {
      this.logger.info('initialising request body parser...');
      this.initialiseBodyParser();
    }

    // request tracing handling
    if (serverTracingEnabled) {
      this.logger.info('initialising request tracing...');
      this.initialiseTracing();
    }

    // request logging handling
    this.logger.info('initialising request logging');
    this.initialiseRequestLogging();

    // browser security
    if (serverSecureHttpHeadersEnabled) {
      this.logger.info('initialising http header security...');
      this.initialiseSecureHttpHeaders();
    }

    // cookies handling
    if (serverCookiesEnabled) {
      this.logger.info('initialising cookie compatibility...');
      this.initialiseCookies();
    }

    // cors handling
    if (serverCorsEnabled) {
      this.logger.info('initialising cross origin resource sharing (cors) support...');
      this.initialiseCors();
    }
    
    // csp handling
    if (serverCspEnabled) {
      this.logger.info('initialising content security policy (csp) support...');
      this.initialiseCsp();
    }
    
    // metrics handling
    if (serverMetricsEnabled) {
      this.logger.info('initialising metrics collection/serving...');
      this.initialiseMetrics();
    }

    // reporting url handling
    const reportUriStub = path.join(serverReportUri, '/:type');
    this.app.use(reportUriStub, (req, res) => {
      const {type} = req.params;
      this.logger.error(
        `received browser report for ${type}`,
        {
          data: req.body,
          type,
        },
      );
      res.status(200);
      res.json('ok');
    });
    this.logger.debug(`registered browser reporting endpoint "${reportUriStub}/:type" `);

    // graceful shutdown handling
    this.onShutdown = serverShutdownHooks;
    this.logger.info(`registered ${this.onShutdown.length} shutdown hooks`);

    // it is done
    return this;
  }

  verifyCorsAllowsMethod(method: string): boolean {
    return this.config.get('serverCorsEnabled')
    && this.config
      .get('serverCorsHttpVerbWhitelist')
      .split(',')
      .map((verb) => verb.toLowerCase()).indexOf(method) === -1;
  }

  get(...args: ExpressHandlerParameters[]): Server {
    this.app.get.apply(this.app, args);
    if (typeof args[0] === 'string') {
      if (this.verifyCorsAllowsMethod('get')) {
        this.logger.warn(`GET ${args[0]} might be blocked by your cors policy!`);
      }
      this.logger.debug(`GET ${args[0]} registered`);
    }
    return this;
  }

  post(...args: ExpressHandlerParameters[]): Server {
    this.app.post.apply(this.app, args);
    if (typeof args[0] === 'string') {
      if (this.verifyCorsAllowsMethod('post')) {
        this.logger.warn(`POST ${args[0]} might be blocked by your cors policy!`);
      }
      this.logger.debug(`POST ${args[0]} registered`);
    }
    return this;
  }

  put(...args: ExpressHandlerParameters[]): Server {
    this.app.put.apply(this.app, args);
    if (typeof args[0] === 'string') {
      if (this.verifyCorsAllowsMethod('put')) {
        this.logger.warn(`PUT ${args[0]} might be blocked by your cors policy!`);
      }
      this.logger.debug(`PUT ${args[0]} registered`);
    }
    return this;
  }

  patch(...args: ExpressHandlerParameters[]): Server {
    this.app.patch.apply(this.app, args);
    if (typeof args[0] === 'string') {
      if (this.verifyCorsAllowsMethod('patch')) {
        this.logger.warn(`PATCH ${args[0]} might be blocked by your cors policy!`);
      }
      this.logger.debug(`PATCH ${args[0]} registered`);
    }
    return this;
  }

  delete(...args: ExpressHandlerParameters[]): Server {
    this.app.delete.apply(this.app, args);
    if (typeof args[0] === 'string') {
      if (this.verifyCorsAllowsMethod('delete')) {
        this.logger.warn(`DELETE ${args[0]} might be blocked by your cors policy!`);
      }
      this.logger.debug(`DELETE ${args[0]} registered`);
    }
    return this;
  }

  head(...args: ExpressHandlerParameters[]): Server {
    this.app.head.apply(this.app, args);
    if (typeof args[0] === 'string') {
      if (this.verifyCorsAllowsMethod('head')) {
        this.logger.warn(`HEAD ${args[0]} might be blocked by your cors policy!`);
      }
      this.logger.debug(`HEAD ${args[0]} registered`);
    }
    return this;
  }

  options(...args: ExpressHandlerParameters[]): Server {
    this.app.options.apply(this.app, args);
    if (typeof args[0] === 'string') {
      if (this.verifyCorsAllowsMethod('options')) {
        this.logger.warn(`OPTIONS ${args[0]} might be blocked by your cors policy!`);
      }
      this.logger.debug(`OPTIONS ${args[0]} registered`);
    }
    return this;
  }

  /**
   * Pass-through function for Express's .use
   *
   * @param {ExpressHandlerParameters[]} args
   * @return {Server}
   */
  use(...args: ExpressHandlerParameters[]): Server {
    this.app.use.apply(this.app, args);
    return this;
  }

  /**
   * Pass-through function for Express's .set
   *
   * @param {ExpressHandlerParameters[]} args
   * @return {Server}
   */
  set(...args: ExpressHandlerParameters[]): Server {
    this.app.set.apply(this.app, args);
    return this;
  }

  /**
   * Pass-through function for Express's .engine
   *
   * @param {any[]} args
   * @return {Server}
   */
  engine(...args): Server {
    this.app.engine.apply(this.app, args);
    return this;
  }

  /**
   * Returns the Express application
   */
  ejectServer(): express.Application {
    return this.app;
  }

  /**
   * Initialises the body parser middleware
   */
  initialiseBodyParser(): void {
    try {
      const bodyParser = new BodyParser();
      this.app.use(bodyParser.getMiddlewares());
      this.logger.debug(`initialised body parsing for ${bodyParser.types} of sizes up to ${bodyParser.sizeLimit}`);
    } catch (ex) {
      this.logger.error(`failed to initialise body parsing: ${ex}`);
    }
  }

  /**
   * Initialises cookie parsing capabilities
   */
  initialiseCookies(): void {
    try {
      this.app.use(cookieParser());
      this.logger.debug('initialised cookie parsing');
    } catch (ex) {
      this.logger.error(`failed to initialise cookie handling: ${ex}`);
    }
  }

  /**
   * Initialises the cross origin resource sharing (CORS) functionality
   */
  initialiseCors(): void {
    try {
      const serverCorsHeaderWhitelist = this.config.get('serverCorsHeaderWhitelist');
      const serverCorsHttpVerbWhitelist = this.config.get('serverCorsHttpVerbWhitelist');
      const serverCorsUrlWhitelist = this.config.get('serverCorsUrlWhitelist');
      const serverPublicUrl = this.config.get('serverPublicUrl');
      this.corsUrlWhitelist = (serverCorsUrlWhitelist.length === 0) ?
        [serverPublicUrl] : serverCorsUrlWhitelist.split(',');
      this.logger.info(`registering cors url whitelist for: ${this.corsUrlWhitelist.join(', ')}`);
      this.app.use(cors({
        allowedHeaders: serverCorsHeaderWhitelist,
        credentials: true,
        methods: serverCorsHttpVerbWhitelist,
        origin: (origin, callback) => {
          const valid = (origin === undefined) || (this.corsUrlWhitelist.indexOf(origin) !== -1);
          const error = Object.assign(
            new Error('Not allowed by CORS'), {
            source: 'cors',
          });
          callback(valid ? null : error, valid);
        },
        maxAge: common.time.sixtyDaysInSeconds,
        optionsSuccessStatus: 200,
      }));
      this.logger.debug('initialised cross origin resource sharing (cors)');
    } catch (ex) {
      this.logger.error(`failed to initialise cross origin resource sharing: ${ex}`);
    }
  }

  initialiseCsp(): void {
    try {
      const csp = new Csp();
      this.app.use(csp.getMiddleware());
      this.logger.debug('initialised content security policy (csp) support');
    } catch (ex) {
      this.logger.error(`failed to initialise content security policy: ${ex}`);
    }
  }

  initialiseMetrics(): void {
    try {
      const serverMetricsUri = this.config.get('serverMetricsUri');
      const metrics = expressPromBundle({
        autoregister: false,
        includeMethod: true,
        includeStatusCode: true,
        includePath: true,
        includeUp: true,
        promClient: {
          collectDefaultMetrics: true,
        },
      });
      const getParameters = [serverMetricsUri];
      if (this.config.get('serverMetricsBasicAuthEnabled')) {
        const serverMetricsBasicAuthCredentials =
          this.config.get('serverMetricsBasicAuthCredentials');
        if (serverMetricsBasicAuthCredentials.length >= 3) {
          const credentials = serverMetricsBasicAuthCredentials.split(':');
          const username = credentials[0];
          const password = credentials[1];
          getParameters.push((req, res, next) => {
            const credentials = basicAuth(req);
            if (
              !credentials
              || credentials.name !== username
              || credentials.pass !== password
            ) {
              res.status(401);
              res.header('WWW-Authenticate', 'Basic realm="metrics"');
              res.end();
            } else {
              next(); 
            }
          });
        }
      }
      getParameters.push(metrics.metricsMiddleware);
      this.app.get.apply(this.app, getParameters);
      this.logger.debug(`initialised metrics collection accessible at ${serverMetricsUri}`);
    } catch (ex) {
      this.logger.error(`failed to initialise metrics: ${ex}`);
    }
  }

  /**
   * Initialises the logger for incoming requests
   */
  initialiseRequestLogging(): void {
    try {
      this.app.use(morgan('combined', {
        immediate: false,
        stream: {
          write: (message) => {
            const trace = this.context.active ? {
              spanId: this.context.active.zipkin.spanId,
              parentId: this.context.active.zipkin.parentId,
              traceId: this.context.active.zipkin.traceId,
            } : {};
            this.logger.info(message, {type: 'access', trace});
          },
        },
      }));
      this.logger.debug('initialised request logging');
    } catch (ex) {
      this.logger.error(`failed to initialise request logging: ${ex}`);
    }
  }

  /**
   * Initialises request tracing
   */
  initialiseTracing(): void {
    try {
      // zipkinConsole.error = this.logger.error;
      const recorder = this.config.get('serverTracingRemoteEnabled') ?
        new zipkin.BatchRecorder({
          logger: new HttpLogger({
            endpoint: this.config.get('serverTracingRemoteUrl'),
            httpInterval: 1000,
            jsonEncoder: zipkin.jsonEncoder.JSON_V2,
            log: new Console(
              new stream.Duplex({write: (message) => this.logger.info(message)}),
              new stream.Duplex({write: (message) => this.logger.warn(message)}),
              false
            ),
          }),
        })
        : new zipkin.ConsoleRecorder();
      this.tracer = new zipkin.Tracer({
        recorder,
        ctxImpl: new zipkinContextCls(this.config.get('serverServiceId')),
        sampler: new zipkin.sampler.CountingSampler(1),
        traceId128Bit: true,
        localServiceName: this.config.get('serverServiceId'),
      });
      this.app.use(zipkinExpressMiddleware({tracer: this.tracer}));
      this.logger.debug('initialised tracer');
    } catch (ex) {
      this.logger.error(`failed to initialise request tracing: ${ex}`);
    }
  }

  /**
   * Initialises some basic http header security using helmet
   */
  initialiseSecureHttpHeaders(): void {
    try {
      const serverReportUri = this.config.get('serverReportUri');
      this.app.use(
        helmet.permittedCrossDomainPolicies({
          permittedPolicies: 'none',
        }),
        helmet.dnsPrefetchControl({
          allow: false,
        }),
        helmet.expectCt({
          enforce: true,
          maxAge: 30,
          reportUri: path.join(serverReportUri, '/ct'),
        }),
        featurePolicy({
          features: {
            fullscreen: ["'none'"],
            notifications: ["'none'"],
            vibrate: ["'none'"],
            payment: ["'none'"],
            syncXhr: ["'none'"],
          },
        }),
        helmet.frameguard({
          action: 'deny',
        }),
        helmet.hidePoweredBy(),
        helmet.hsts({
          includeSubdomains: true,
          maxAge: common.time.sixtyDaysInSeconds,
          setIf: (req) => req.secure,
        }),
        helmet.ieNoOpen(),
        helmet.noCache(),
        helmet.noSniff(),
        xssFilter({
          setOnOldIE: true,
          reportUri: path.join(serverReportUri, '/xss'),
        }),
      );
      this.logger.debug('initialised http header security');
    } catch (ex) {
      this.logger.error(`failed to initialise http header security: ${ex}`);
    }
  }

  /**
   * Starts listening on the interface and port as provided by the configuration
   *
   * @param {ServerListenCallback} callback - overrides the default listen callback
   */
  listen(callback?: ServerListenCallback): void {
    this.app.use((err, req, res, next) => {
      if (err['source'] && err['source'] === 'cors') {
        res.status(err['status'] || 500);
        res.end();
      }
    });
    this.logger.info(`starting server with id ${this.id}`);
    this.logger.info(`attempting to listen on ${this.config.get('serverInterface')}:${this.config.get('serverPort')}`);
    this.instance = this.app.listen(
      this.config.get('serverPort'),
      this.config.get('serverInterface'),
      callback ? (err) => {
        callback(this.instance, err);
      }: this.listenCallback.bind(this),
    );
    process.on('SIGINT', this.shutdown.bind(this, 'SIGINT'));
    process.on('SIGTERM', this.shutdown.bind(this, 'SIGTERM'));
  }

  /**
   * Internal use function handler
   * 
   * @param {Error} error - an error returned by the callback of listen()
   */
  listenCallback(error: Error): void {
    if (error !== undefined) {
      this.logger.error(error);
    } else {
      const listeningAddress = this.instance.address();
      const startupDuration = (new Date()).getTime() - this.startTime.getTime();
      // tslint:disable-next-line max-line-length - log statement
      this.logger.info(`started server with id ${this.id} in ${startupDuration} milliseconds listening on ${listeningAddress['address']}:${listeningAddress['port']}`);
      if (startupDuration > 5000) {
        // tslint:disable-next-line max-line-length - log statement
        this.logger.warn('application took longer than 5 seconds to start up, some optimisations maybe due, just saying');
      }
    }
  }

  /**
   * Handles the shutdown hooks
   *
   * @param {String} signal
   */
  shutdown(signal: string) {
    this.logger.info(`received signal ${signal}, terminating server with id ${this.id}`);
    Promise.all(this.onShutdown.map((shutdownHook) => shutdownHook(this, signal)))
      .then(() => {
      if (this.instance) {
        this.instance.close((err) => {
          this.logger.info(`server with id ${this.id} has shut down`);
        });
      }
    });
  }
}

export interface ServerListenCallback {
  (server: http.Server, error: Error): void;
}

export interface ServerShutdownHook {
  (server: Server, signal: string): Promise<void>;
}
