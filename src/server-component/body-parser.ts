import {RequestHandler} from 'express';
import {config} from './config';
import * as bodyParser from 'body-parser';

export interface BodyParserOptions {
  limit?: string;
  types?: string[];
}

export class BodyParser {
  sizeLimit: string;
  types: string[];

  constructor() {
    this.sizeLimit = config.get('serverBodyParserSizeLimit');
    this.types = config.get('serverBodyParserType').split(',');
    if (config.get('serverCspEnabled')) {
      this.types.push('*/csp-report');
    }
    return this;
  }

  getMiddlewares(): RequestHandler[] {
    return [
      bodyParser.json({
        limit: this.sizeLimit,
        type: this.types,
      }),
      bodyParser.urlencoded({
        extended: true,
      }),
    ];
  }
}
