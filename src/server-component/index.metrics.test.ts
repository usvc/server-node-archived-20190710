import {expect} from 'chai';
import * as supertest from 'supertest';
import * as expressPromBundle from 'express-prom-bundle';

import {Server} from '~';

describe('metrics', () => {
  let server: Server;
  let testServer: supertest.SuperTest<supertest.Test>;
  
  before(() => {
    expressPromBundle.promClient.register.clear();
    server = new Server({
      serverLogLevel: 'error',
    });
    server.get('/', (_req, res) => res.json('ok'));
    testServer = supertest(server.ejectServer());
  });

  it('has a provisioned endpoint', () =>
    testServer.get('/')
      .then(() => testServer.get('/_/metrics'))
      .then((res) => {
        expect(res.text)
          .to.contain('nodejs_active_requests_total')
          .and.to.contain('nodejs_active_handles_total')
          .and.to.contain('nodejs_eventloop_lag_seconds')
          .and.to.contain('nodejs_heap_size_total_bytes')
          .and.to.contain('nodejs_version_info')
          .and.to.contain('up 1');
      })
  );

  it('can be handled at another endpoint', () => {
    expressPromBundle.promClient.register.clear();
    server = new Server({
      serverMetricsUri: '/metrics',
      serverLogLevel: 'error',
    });
    server.get('/', (_req, res) => res.json('ok'));
    return supertest(server.ejectServer())
      .get('/metrics')
      .then((res) => {
        expect(res.text).to.contain('up 1');
      });
  });
});
