import {expect} from 'chai';
import * as supertest from 'supertest';
import * as expressPromBundle from 'express-prom-bundle';

import {Server} from '~';

describe('security headers', () => {
  let server: Server;
  let testServer: supertest.SuperTest<supertest.Test>;
  let baseResponse: supertest.Response;
  
  before(() => {
    expressPromBundle.promClient.register.clear();
    server = new Server({
      serverLogLevel: 'error',
    });
    testServer = supertest(server.ejectServer());
    return testServer.get('/').then((res) => (baseResponse = res));
  });

  it('does not support cross domain policies', () =>
    expect(baseResponse.header['x-permitted-cross-domain-policies'])
      .to.equal('none'),
  );

  it('turns off dns prefetch controls', () =>
    expect(baseResponse.header['x-dns-prefetch-control'])
      .to.equal('off'),
  );

  it('expects certificate transparency', () =>
    expect(baseResponse.header['expect-ct'])
      .to.contain('enforce'),
  );

  it('sets the certificate transparency reporting url correctly', () =>
    expect(baseResponse.header['expect-ct'])
      .to.contain('report-uri="/_/report/ct"'),
  );

  it('denies full-screen feature policies', () =>
    expect(baseResponse.header['feature-policy'])
      .to.contain('fullscreen \'none\'')
  );

  it('denies notification feature policies', () =>
    expect(baseResponse.header['feature-policy'])
      .to.contain('notifications \'none\'')
  );

  it('denies vibration feature policies', () =>
    expect(baseResponse.header['feature-policy'])
      .to.contain('vibrate \'none\'')
  );

  it('denies payment feature policies', () =>
    expect(baseResponse.header['feature-policy'])
      .to.contain('payment \'none\'')
  );

  it('denies sync xhr feature policies', () =>
    expect(baseResponse.header['feature-policy'])
      .to.contain('sync-xhr \'none\'')
  );

  it('prevents usage in an iframe', () =>
    expect(baseResponse.header['x-frame-options'])
      .to.equal('DENY')
  );

  it('prevents auto-downloads in IE', () =>
    expect(baseResponse.header['x-download-options'])
      .to.equal('noopen')
  );

  it('prevents resource caching', () =>
    expect(baseResponse.header['cache-control'])
      .to.contain('no-store')
      .and.contains('no-cache')
      .and.contains('must-revalidate')
      .and.contains('proxy-revalidate')
  );

  it('implemenets cross site scripting protection', () =>
    expect(baseResponse.header['x-xss-protection'])
      .to.contain('1')
      .and.contains('mode=block')
  );

  it('sets the reporting url correctly for cross site scripting reports', () =>
    expect(baseResponse.header['x-xss-protection'])
      .to.contain('/_/report/xss')
  );

  it('implements csp', () => {
    expect(baseResponse.header['x-content-security-policy']).to.not.equal(undefined);
    expect(baseResponse.header['x-webkit-csp']).to.not.equal(undefined);
  });

  it('implements a tightly closed csp by default', () => {
    const expectedCsp =
      'block-all-mixed-content; child-src \'self\'; default-src \'self\'; font-src \'self\'; frame-src \'self\'; '
    + 'img-src \'self\'; manifest-src \'self\'; media-src \'self\'; object-src \'self\'; prefetch-src \'self\'; '
    + 'script-src \'self\'; style-src \'self\'; report-uri /_/report/csp; upgrade-insecure-requests; '
    + 'worker-src \'self\'';
    expect(baseResponse.header['x-content-security-policy']).to.equal(expectedCsp);
    expect(baseResponse.header['x-webkit-csp']).to.equal(expectedCsp);
  });

  it('hides the powered by', () =>
    expect(baseResponse.header['x-powered-by'])
      .to.be.undefined
  );
});
