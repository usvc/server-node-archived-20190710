export const config = {
  serverBodyParserEnabled: {
    description: 'Indicates whether we should parse a body',
    default: true,
    env: 'SERVER_BODY_PARSER_ENABLED',
  },
  serverBodyParserSizeLimit: {
    descirption: 'Maximum size of the POST body',
    default: '300kb',
    env: 'SERVER_BODY_PARSER_SIZE_LIMIT',
  },
  serverBodyParserType: {
    description: 'HTTP "content-type" header to match before doing a JSON parse',
    default: '*/json',
    env: 'SERVER_BODY_PARSER_TYPE',
  },
};
