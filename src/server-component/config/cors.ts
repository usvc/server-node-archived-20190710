export const config = {
  serverCorsEnabled: {
    description: 'Indicates whether cors should be enabled',
    default: true,
    env: 'SERVER_CORS_ENABLED',
  },
  serverCorsHeaderWhitelist: {
    description: 'Comma separated list of HTTP headers to whitelist for cross origin resource sharing (CORS)',
    default: 'Content-Type,X-Content-Type,Content-Range,X-Content-Range,Authorization,X-Authorization,Cookie',
    env: 'SERVER_CORS_HEADER_WHITELIST',
  },
  serverCorsHttpVerbWhitelist: {
    description: 'Comma separated list of HTTP verbs to whitelist for cross origin resource sharing (CORS)',
    default: 'GET,POST,PUT,PATCH,OPTIONS,HEAD',
    env: 'SERVER_CORS_HTTP_VERB_WHITELIST',
  },
  serverCorsUrlWhitelist: {
    description: 'Comma separated list of URLs to whitelist for cross origin resource sharing (CORS)',
    default: '',
    env: 'SERVER_CORS_URL_WHITELIST',
  },
};
