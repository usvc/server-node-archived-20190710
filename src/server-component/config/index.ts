import * as convict from 'convict';
import {config as serverBodyParserConfig} from './body-parser';
import {config as serverCorsConfig} from './cors';
import {config as serverCspConfig} from './csp';
import {config as serverMetricsConfig} from './metrics';
import {DEFAULT} from '../../common';

export const configDefinition = Object.assign({},
  serverBodyParserConfig,
  serverCorsConfig,
  serverCspConfig,
  {
    serverCookiesEnabled: {
      description: 'Indicates whether cookies should be enabled',
      default: true,
      env: 'SERVER_COOKIES_ENABLED',
    },
    serverInterface: {
      description: 'Interface the server should listen on',
      default: '0.0.0.0',
      env: 'SERVER_INTERFACE',
    },
    serverLogLevel: {
      // tslint:disable-next-line max-line-length - description
      description: 'Indiciates the level of logs which the server should display (eg. silly, debug, verbose, info, warn, error)',
      default: 'silly',
      format: ['silly', 'debug', 'verbose', 'info', 'warn', 'error'],
      env: 'SERVER_LOG_LEVEL',
    },
  },
  serverMetricsConfig,
  {
    serverPort: {
      description: 'Port the server should listen on',
      default: 8080,
      env: 'SERVER_PORT',
    },
    serverPublicUrl: {
      description: 'Public address the server will be accessible at',
      default: 'http://localhost:8080',
      env: 'SERVER_PUBLIC_URL',
    },
    serverReportUri: {
      description: 'Path stub for reporting URIs',
      default: '/_/report',
      env: 'SERVER_REPORT_URI',
    },
    serverTracingEnabled: {
      description: 'Indicates whether the server should report request traces',
      default: true,
      env: 'SERVER_TRACING_ENABLED',
    },
    serverTracingRemoteEnabled: {
      description: 'Indicates whether to use a HTTP transport',
      default: true,
      env: 'SERVER_TRACING_REMOTE_ENABLED',
    },
    serverTracingRemoteUrl: {
      description: 'The reporting URL for the traces',
      default: 'http://zipkin:9411',
      env: 'SERVER_TRACING_REMOTE_URL',
    },
    serverSecureHttpHeadersEnabled: {
      description: 'Indicates whether http header security is implemented (usually for front-end only)',
      default: true,
      env: 'SERVER_SECURITY_BROWSER_ENABLED',
    },
    serverServiceId: {
      description: 'The name of the application/service this server hosts',
      default: DEFAULT.SERVICE_ID,
      env: 'SERVER_SERVICE_ID',
    },
  },
);

export const config = convict(configDefinition);
