export const config = {
  serverMetricsEnabled: {
    description: 'Indicates whether the server should report metrics',
    default: true,
    env: 'SERVER_METRICS_ENABLED',
  },
  serverMetricsUri: {
    description: 'URI path for the metrics endpoint',
    default: '/_/metrics',
    env: 'SERVER_METRICS_URI',
  },
  serverMetricsBasicAuthEnabled: {
    description: 'Indicates whether the metrics endpoint should be protected by Basic Auth',
    default: true,
    env: 'SERVER_METRICS_BASIC_AUTH_ENABLED',
  },
  serverMetricsBasicAuthCredentials: {
    description: 'Credentials in `<USERNAME>:<PASSWORD>` format',
    default: '',
    env: 'SERVER_METRICS_BASIC_AUTH_CREDENTIALS',
  },
};
