export const config = {
  serverCspEnabled: {
    description: 'Indicates whether csp should be enabled',
    default: true,
    env: 'SERVER_CSP_ENABLED',
  },
  serverCspBlockMixedContent: {
    description: 'Indicates whether CSP should cause all http/https mixed elements to be blocked',
    default: true,
    env: 'SERVER_CSP_BLOCK_MIXED_CONTENT',
  },
  serverCspChild: {
    description: 'Comma separated list of values to use for the `child-src` CSP property',
    default: '\'self\'',
    env: 'SERVER_CSP_CHILD',
  },
  serverCspConnect: {
    description: 'Comma separated list of values to use for the `connect-src` CSP property',
    default: '\'self\'',
    env: 'SERVER_CSP_CONNECT',
  },
  serverCspDefault: {
    description: 'Comma separated list of values to use for the `default-src` CSP property',
    default: '\'self\'',
    env: 'SERVER_CSP_DEFAULT',
  },
  serverCspFont: {
    description: 'Comma separated list of values to use for the `font-src` CSP property',
    default: '\'self\'',
    env: 'SERVER_CSP_FONT',
  },
  serverCspFrame: {
    description: 'Comma separated list of values to use for the `frame-src` CSP property',
    default: '\'self\'',
    env: 'SERVER_CSP_FRAME',
  },
  serverCspImg: {
    description: 'Comma separated list of values to use for the `img-src` CSP property',
    default: '\'self\'',
    env: 'SERVER_CSP_IMG',
  },
  serverCspManifest: {
    description: 'Comma separated list of values to use for the `manifest-src` CSP property',
    default: '\'self\'',
    env: 'SERVER_CSP_MANIFEST',
  },
  serverCspMedia: {
    description: 'Comma separated list of values to use for the `media-src` CSP property',
    default: '\'self\'',
    env: 'SERVER_CSP_MEDIA',
  },
  serverCspObject: {
    description: 'Comma separated list of values to use for the `object-src` CSP property',
    default: '\'self\'',
    env: 'SERVER_CSP_OBJECT',
  },
  serverCspPrefetch: {
    description: 'Comma separated list of values to use for the `prefetch-src` CSP property',
    default: '\'self\'',
    env: 'SERVER_CSP_PREFETCH',
  },
  serverCspScript: {
    description: 'Comma separated list of values to use for the `script-src` CSP property',
    default: '\'self\'',
    env: 'SERVER_CSP_SCRIPT',
  },
  serverCspStyle: {
    description: 'Comma separated list of values to use for the `style-src` CSP property',
    default: '\'self\'',
    env: 'SERVER_CSP_STYLE',
  },
  serverCspUpgradeInsecure: {
    description: 'Indicates whether to upgrade all HTTP requests to HTTPS',
    default: true,
    env: 'SERVER_CSP_UPGRADE_INSECURE',
  },
  serverCspWorker: {
    description: 'Comma separated list of values to use for the `worker-src` CSP property',
    default: '\'self\'',
    env: 'SERVER_CSP_WORKER',
  },
};
