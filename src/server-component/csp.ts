import * as path from 'path';
import * as helmet from 'helmet';
import {RequestHandler} from 'express';
import {config} from './config';

export interface CspDirectives {
  blockAllMixedContent: boolean;
  childSrc: string[];
  defaultSrc: string[];
  fontSrc: string[];
  frameSrc: string[];
  imgSrc: string[];
  manifestSrc: string[];
  mediaSrc: string[];
  objectSrc: string[];
  prefetchSrc: string[];
  scriptSrc: string[];
  styleSrc: string[];
  reportUri: string;
  upgradeInsecureRequests: boolean;
  workerSrc: string[];
}

export class Csp {
  directives: CspDirectives;

  constructor() {
    this.directives = {
      blockAllMixedContent: config.get('serverCspBlockMixedContent'),
      childSrc: [].concat(config.get('serverCspChild').split(',')),
      defaultSrc: [].concat(config.get('serverCspDefault').split(',')),
      fontSrc: [].concat(config.get('serverCspFont').split(',')),
      frameSrc: [].concat(config.get('serverCspFrame').split(',')),
      imgSrc: [].concat(config.get('serverCspImg').split(',')),
      manifestSrc: [].concat(config.get('serverCspManifest').split(',')),
      mediaSrc: [].concat(config.get('serverCspMedia').split(',')),
      objectSrc: [].concat(config.get('serverCspObject').split(',')),
      prefetchSrc: [].concat(config.get('serverCspPrefetch').split(',')),
      scriptSrc: [].concat(config.get('serverCspScript').split(',')),
      styleSrc: [].concat(config.get('serverCspStyle').split(',')),
      reportUri: path.join(config.get('serverReportUri'), '/csp'),
      upgradeInsecureRequests: config.get('serverCspUpgradeInsecure'),
      workerSrc: [].concat(config.get('serverCspWorker').split(',')),
    };
    return this;
  }

  getMiddleware(): RequestHandler {
    return helmet.contentSecurityPolicy({
      directives: this.directives,
      browserSniff: true,
    });
  }
}
