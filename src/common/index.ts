export const time = {
  oneMinuteInSeconds: 60,
  oneHourInSeconds: 60 * 60,
  oneDayInSeconds: 60 * 60 * 24,
  thirtyDaysInSeconds: 60 * 60 * 24 * 30,
  sixtyDaysInSeconds: 60 * 60 * 24 * 60,
  oneYearInSeconds: 60 * 60 * 24 * 365,
};

export const DEFAULT = {
  CONTEXT_NAME: 'unknown-context',
  REMOTE_SERVICE_ID: 'unknown-remote',
  SERVICE_ID: 'unknown',
};
