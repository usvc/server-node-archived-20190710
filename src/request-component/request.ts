import * as zipkin from 'zipkin';
import * as fetch from 'node-fetch';
import * as request from 'request';
import zipkinInstrumentationFetch = require('zipkin-instrumentation-fetch');
import zipkinInstrumentationRequest = require('zipkin-instrumentation-request');
import {DEFAULT} from '../common';

export interface RequestConstructorParameters {
  remoteServiceName?: string;
  tracer?: zipkin.Tracer;
}

export class Request {
  fetchModule: (url: string, opts: fetch.RequestInit) => Promise<fetch.Response>;
  remoteServiceName: string;
  requestModule: request.RequestAPI<request.Request, request.CoreOptions, request.RequiredUriUrl>;
  tracer: zipkin.Tracer;

  /**
   * 
   * @param {RequestConstructorParameters} opts
   * @param {string} opts.remoteServiceName
   * @param {zipkin.Tracer} opts.tracer
   */
  constructor({
    remoteServiceName = DEFAULT.REMOTE_SERVICE_ID,
    tracer,
  }: RequestConstructorParameters = {}) {
    this.remoteServiceName = remoteServiceName;
    this.tracer = tracer;
    if (tracer === undefined) {
      throw new Error('the tracer parameter is a required field');
    }
    const instrumentationOptions = {
      tracer,
      remoteServiceName,
    };
    this.fetchModule = zipkinInstrumentationFetch(
      fetch.default,
      instrumentationOptions,
    );
    this.requestModule = zipkinInstrumentationRequest(
      request,
      instrumentationOptions,
    );
    return this;
  }

  /**
   * Pass-through for the `fetch` interface.
   *
   * @param {[]any} args
   * @see https://www.npmjs.com/package/node-fetch
   */
  fetch(...args) {
    if (!this.fetchModule) {
      throw new Error('the `fetch` module was not initialised');
    }
    this.fetchModule.apply(this.fetchModule, args);
  }

  request(...args) {
    if (!this.requestModule) {
      throw new Error('the `request` module was not initialised');
    }
    this.requestModule.apply(this.requestModule, args);
  }
}