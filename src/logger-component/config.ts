import * as convict from 'convict';

export const configDefinition = {
  logColor: {
    description: 'Indicates whether to colorize the logs',
    default: true,
    env: 'LOG_COLOR',
  },
  logFormat: {
    description: 'Format of the logs according to Winston (eg. "simple", "json", "logstash)',
    format: ['simple', 'json', 'logstash'],
    default: 'simple',
    env: 'LOG_FORMAT',
  },
  logLevel: {
    // tslint:disable-next-line max-line-length - its a description
    description: 'Indicates which level of logs do we want to see (follows npm levels: silly, debug, verbose, info, warn, error',
    format: ['silly', 'debug', 'verbose', 'info', 'warn', 'error'],
    default: 'silly',
    env: 'LOG_LEVEL',
  },
};

export const config = convict(configDefinition);
