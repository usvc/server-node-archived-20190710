import {
  LoggerInitOptions,
  logger,
} from './logger';

export {
  LoggerInitOptions,
  logger,
};
