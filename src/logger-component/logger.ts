import * as os from 'os';
import * as winston from 'winston';
import * as clsHooked from 'cls-hooked';
import {config} from './config';
import {Format} from 'logform';
import {DEFAULT} from '../common';

export interface LoggerInitOptions {
  additionalFormatters?: Format[];
  additionalTransports?: winston.transports.StreamTransportInstance[];
  color?: boolean;
  format?: string;
  level?: string;
}

export const logger = {
  zipkinContextFormatter:
    (
      contextName: string = DEFAULT.CONTEXT_NAME,
    ) => {
      const context = clsHooked.getNamespace(contextName);
      return winston.format((info) =>
        context.active && context.active.zipkin ?
          Object.assign(info, {
            parentId: context.active.zipkin.parentId,
            spanId: context.active.zipkin.spanId,
            traceId: context.active.zipkin.traceId,
            sampled: context.active.zipkin.sampled,
          }) : info
      )();
    },
  init:
    (
      moduleName: string,
      {
        additionalFormatters = [],
        additionalTransports = [],
        color = config.get('logColor'),
        format = config.get('logFormat'),
        level = config.get('logLevel'),
      }: LoggerInitOptions = {},
    ) => {
      config.set('logColor', color);
      config.set('logFormat', format);
      config.set('logLevel', level);
      const logFormat = winston.format[config.get('logFormat')]();
      const logColor = config.get('logColor') ?
        (config.get('logFormat') !== 'simple' ?
          winston.format.uncolorize()
          : winston.format.colorize()
        )
        : winston.format.uncolorize();
      return winston.createLogger({
        level: config.get('logLevel'),
        format: winston.format.combine(
          logColor,
          winston.format.timestamp(),
          winston.format.splat(),
          ...additionalFormatters,
        ),
        defaultMeta: {
          module: moduleName,
          hostname: os.hostname(),
        },
        transports: [
          new winston.transports.Console({
            format: logFormat,
          }),
          ...additionalTransports,
        ],
      });
    },
  };
