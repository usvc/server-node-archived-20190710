(function() {
  fetch('http://localhost:8080/hello')
    .then((value) => value.json())
    .then((body) => {
      console.info('# text body');
      console.info(body);
      document.getElementById('sample-text').append(body);
    });

  fetch('http://localhost:8080/cookies', {
      credentials: 'include',
    })
    .then((value) => value.json())
    .then((body) => {
      console.info('# cookies');
      console.info(body);
    });

  fetch('http://localhost:8080/post', {
      method: 'POST',
    })
    .then((value) => value.json())
    .then((body) => {
      console.info('# post');
      console.info(body);
    });

  fetch('http://localhost:8080/put', {
      method: 'PUT',
      body: 'hello from the client side!',
    })
    .then((value) => value.json())
    .then((body) => {
      console.info('# put');
      console.info(body);
    });

  fetch('http://localhost:8080/patch', {
      method: 'PATCH',
      body: 'hello from the client side!',
    })
    .then((value) => value.json())
    .then((body) => {
      console.info('# patch');
      console.info(body);
    });
})();
