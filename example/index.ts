import path = require('path');
import * as ejs from 'ejs';
import * as express from 'express';
import {Server} from '../src';

const server = new Server({
  // change this to another url to see CSP in action
  serverCorsUrlWhitelist: 'http://localhost:8080',
  // use a PUT request to see CSP in action
  serverCorsHttpVerbWhitelist: 'GET,POST',
  serverCspImg: '\'self\',data:',
  serverMetricsBasicAuthCredentials: 'user:pass',
});

server
  .set('view engine', 'ejs')
  .engine('html', ejs.renderFile)
  .get('/render', (req, res) => {
    res.render(path.join(__dirname, '/static/index.html'), {
      text: 'hello from the server side!'
    });
  })
  .use('/static', express.static(path.join(__dirname, '/static')))
  .get('/hello', (req, res) => {
    res.json('It works!');
  })
  .get('/cookies', (req, res) => {
    res.json(req.cookies);
  })
  // a working POST endpoint, change the 
  // above serverCorsHttpVerbWhitelist to not include
  // POST to view CSP in action
  .post('/post', (req, res) => {
    res.json('ok');
  })
  // this will not work because of cors, also
  // notice the warning log that indicates this
  // endpoint may not be reachable
  .put('/put', (req, res) => {
    res.json(req.body);
  })
  .patch('/patch', (req, res) => {
    res.json(req.body);
  })
  .get('/', (req, res) => {
    server.logger.info('HELLO');
    res.json('hi');
  })
  .listen();
