# Configuration Variables

| Environment Variable | Defaults To | Description |
| --- | --- | --- |
| `LOG_COLOR` | `true` | Indicates whether to colorize the logs |
| `LOG_FORMAT` | `"simple"` | Format of the logs according to Winston (eg. "simple", "json", "logstash) |
| `LOG_LEVEL` | `"silly"` | Indicates which level of logs do we want to see (follows npm levels: silly, debug, verbose, info, warn, error || `SERVER_BODY_PARSER_ENABLED` | `true` | Indicates whether we should parse a body |
| `SERVER_BODY_PARSER_SIZE_LIMIT` | `"300kb"` | undefined |
| `SERVER_BODY_PARSER_TYPE` | `"*/json"` | HTTP "content-type" header to match before doing a JSON parse |
| `SERVER_CORS_ENABLED` | `true` | Indicates whether cors should be enabled |
| `SERVER_CORS_HEADER_WHITELIST` | `"Content-Type,X-Content-Type,Content-Range,X-Content-Range,Authorization,X-Authorization,Cookie"` | Comma separated list of HTTP headers to whitelist for cross origin resource sharing (CORS) |
| `SERVER_CORS_HTTP_VERB_WHITELIST` | `"GET,POST,PUT,PATCH,OPTIONS,HEAD"` | Comma separated list of HTTP verbs to whitelist for cross origin resource sharing (CORS) |
| `SERVER_CORS_URL_WHITELIST` | `""` | Comma separated list of URLs to whitelist for cross origin resource sharing (CORS) |
| `SERVER_CSP_ENABLED` | `true` | Indicates whether csp should be enabled |
| `SERVER_CSP_BLOCK_MIXED_CONTENT` | `true` | Indicates whether CSP should cause all http/https mixed elements to be blocked |
| `SERVER_CSP_CHILD` | `"'self'"` | Comma separated list of values to use for the `child-src` CSP property |
| `SERVER_CSP_CONNECT` | `"'self'"` | Comma separated list of values to use for the `connect-src` CSP property |
| `SERVER_CSP_DEFAULT` | `"'self'"` | Comma separated list of values to use for the `default-src` CSP property |
| `SERVER_CSP_FONT` | `"'self'"` | Comma separated list of values to use for the `font-src` CSP property |
| `SERVER_CSP_FRAME` | `"'self'"` | Comma separated list of values to use for the `frame-src` CSP property |
| `SERVER_CSP_IMG` | `"'self'"` | Comma separated list of values to use for the `img-src` CSP property |
| `SERVER_CSP_MANIFEST` | `"'self'"` | Comma separated list of values to use for the `manifest-src` CSP property |
| `SERVER_CSP_MEDIA` | `"'self'"` | Comma separated list of values to use for the `media-src` CSP property |
| `SERVER_CSP_OBJECT` | `"'self'"` | Comma separated list of values to use for the `object-src` CSP property |
| `SERVER_CSP_PREFETCH` | `"'self'"` | Comma separated list of values to use for the `prefetch-src` CSP property |
| `SERVER_CSP_SCRIPT` | `"'self'"` | Comma separated list of values to use for the `script-src` CSP property |
| `SERVER_CSP_STYLE` | `"'self'"` | Comma separated list of values to use for the `style-src` CSP property |
| `SERVER_CSP_UPGRADE_INSECURE` | `true` | Indicates whether to upgrade all HTTP requests to HTTPS |
| `SERVER_CSP_WORKER` | `"'self'"` | Comma separated list of values to use for the `worker-src` CSP property |
| `SERVER_COOKIES_ENABLED` | `true` | Indicates whether cookies should be enabled |
| `SERVER_INTERFACE` | `"0.0.0.0"` | Interface the server should listen on |
| `SERVER_LOG_LEVEL` | `"silly"` | Indiciates the level of logs which the server should display (eg. silly, debug, verbose, info, warn, error) |
| `SERVER_METRICS_ENABLED` | `true` | Indicates whether the server should report metrics |
| `SERVER_METRICS_URI` | `"/_/metrics"` | URI path for the metrics endpoint |
| `SERVER_METRICS_BASIC_AUTH_ENABLED` | `true` | Indicates whether the metrics endpoint should be protected by Basic Auth |
| `SERVER_METRICS_BASIC_AUTH_CREDENTIALS` | `""` | Credentials in `<USERNAME>:<PASSWORD>` format |
| `SERVER_PORT` | `8080` | Port the server should listen on |
| `SERVER_PUBLIC_URL` | `"http://localhost:8080"` | Public address the server will be accessible at |
| `SERVER_REPORT_URI` | `"/_/report"` | Path stub for reporting URIs |
| `SERVER_TRACING_ENABLED` | `true` | Indicates whether the server should report request traces |
| `SERVER_TRACING_REMOTE_ENABLED` | `true` | Indicates whether to use a HTTP transport |
| `SERVER_TRACING_REMOTE_URL` | `"http://zipkin:9411"` | The reporting URL for the traces |
| `SERVER_SECURITY_BROWSER_ENABLED` | `true` | Indicates whether http header security is implemented (usually for front-end only) |
| `SERVER_SERVICE_ID` | `"unknown"` | The name of the application/service this server hosts |
