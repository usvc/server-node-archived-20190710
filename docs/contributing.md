# Contributing

## Lifecycle
1. Create your own fork
1. Make changes there
1. Issue a pull request back to the `master` branch
1. Wait for the CI pipeline to run
1. Merge!

## Development
### Dependency Installation
We use NPM for this. Run:

```sh
npm install;
```

### Building
To build this package, run:

```sh
npm run build;
```

The distribution files should be made available at `./dist`.

### Docs Generation
To generate the configuration documentation, run:

```sh
npm run make-config-md;
```

### Releasing
This package is available as `@usvc/server` in the NPM repository.

To publish on NPM, run:

```sh
npm publish;
```
